using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Vouchers
{
    public class ConfirmationModel : PageModel
    {
        private readonly DataContext _context;

        public ConfirmationModel(DataContext context)
        {
            _context = context;
        }

        public Voucher Voucher { get; set; }
        public Vendor Vendor { get; set; }

        public async Task<IActionResult> OnGet(string voucherCode)
        {
            Voucher = await _context.Vouchers.FirstOrDefaultAsync(v => v.Code == voucherCode);
            if (Voucher == null) return RedirectToPage("/Errors/NotFound");

            Vendor = await _context.Vendors.FirstOrDefaultAsync(v => v.Id == Voucher.VendorId);
            if (Vendor == null) return RedirectToPage("/Errors/NotFound");

            return Page();
        }    
    }
}