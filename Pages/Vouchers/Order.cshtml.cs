using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Vouchers
{
    public class OrderModel : PageModel
    {
        private readonly DataContext _context;
        
        public OrderModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [BindProperty]
        public Vendor Vendor { get; set; }
        
        [BindProperty]
        public string StripeApi { get; set; }

        public bool PaymentReady { get; set; }
        public string TCContent { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email Address")]
            public string Email { get; set; }

            [Required]
            public string Name { get; set; }

            [Required]
            public double? Amount { get; set; }

            [Required]
            public Address Address { get; set; }

            [Display(Name = "Phone Number")]
            public string PhoneNumber { get; set; }
        }

        public class Address
        {
            [Required]
            public string City { get; set; }

            [Required]
            public string Country { get; set; }

            [Required]
            [Display(Name = "Line 1")]
            public string Line1 { get; set; }

            [Display(Name = "Line 2")]
            public string Line2 { get; set; }

            [Required]
            [Display(Name = "Postal Code")]
            public string PostalCode { get; set; }

            [Required]
            [Display(Name = "County / State")]
            public string County { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int vendorId)
        {
            Vendor = await _context.Vendors.FirstOrDefaultAsync(v => v.Id == vendorId);
            if (Vendor == null) return RedirectToPage("/Errors/NotFound");

            var stripeSettings = (await _context.StripeSettings.FirstOrDefaultAsync());
            StripeApi = stripeSettings?.ApiKey;

            PaymentReady = !string.IsNullOrWhiteSpace(StripeApi) && !string.IsNullOrWhiteSpace(stripeSettings?.SecretKey);

            TCContent = (await _context.TermsAndConditions.FirstOrDefaultAsync())?.Content;
            
            return Page();
        }
    }
}