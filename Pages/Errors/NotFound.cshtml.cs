using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VoucherApp.Pages.Errors
{
    public class NotFound : PageModel
    {
        public IActionResult OnGet()
        {
            return Page();
        }
    }
}