using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Stripe;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Vendors
{
    [Authorize("user")]
    public class IndexModel : PageModel 
    {
        private readonly DataContext _context;

        public IndexModel(DataContext context) 
        {
            _context = context;
        }

        public OutputModel Output { get; set; }
        public StripeSettings StripeSettings { get; set; }
        public bool ValidSettings { get; set; }

        public class OutputModel
        {
            [Display(Name = "Vendor Name")]
            public string Name { get; set; }

            [Display(Name = "Treat Type")]
            public string DessertType { get; set; }

            [Display(Name = "Image")]
            public string ImagePath { get; set; }

            [Display(Name = "Email")]
            public string Email { get; set; }

            [Display(Name = "Stripe Account Id")]
            public string StripeAccountId { get; set; }

            public string State { get; set; }

            public string Description { get; set; }

            [Display(Name = "Social Media Link")]
            public string SocialMediaLink { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(string code, string state) 
        {
            var email = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;
            var vendor = await _context.Vendors.FirstOrDefaultAsync(m => m.UserEmail == email);

            if (vendor == null) return RedirectToPage("/Errors/NotFound");

            // Security check stripe callback
            if (!string.IsNullOrWhiteSpace(state) && !string.IsNullOrWhiteSpace(code))
            {
                switch(await CallbackCheckAsync(vendor, state, code))
                {
                    case StatusCodes.Status401Unauthorized:
                        return Unauthorized();
                    
                    case StatusCodes.Status500InternalServerError:
                        return StatusCode(StatusCodes.Status500InternalServerError, "An unknown error occurred.");

                    default:
                        break;
                }
            }
            else
            {
                // Not callback
                if (string.IsNullOrWhiteSpace(vendor.StripeState))
                {
                    var random = new Random();
                    char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();

                    vendor.StripeState = Enumerable.Range(1, 13)
                        .Select(k => keys[random.Next(0, keys.Length - 1)])
                        .Aggregate("", (e, c) => e + c);

                    await _context.SaveChangesAsync();
                }
            }
            
            StripeSettings = await _context.StripeSettings.FirstOrDefaultAsync();
            ValidSettings = !string.IsNullOrWhiteSpace(StripeSettings?.ClientId);

            Output = new OutputModel
            {
                Name = vendor.Name,
                DessertType = vendor.DessertType,
                Email = vendor.UserEmail,
                StripeAccountId = vendor.StripeAccountId,
                State = vendor.StripeState,
                ImagePath = vendor.ImagePath,
                Description = vendor.Description,
                SocialMediaLink = vendor.SocialMediaLink
            };

            return Page();
        }

        private async Task<int> CallbackCheckAsync(Vendor vendor, string state, string code) 
        {
            if (state != vendor.StripeState) 
            {
                return StatusCodes.Status401Unauthorized;
            }

            return await SendStripeTokenAsync(vendor, code);
        }

        private async Task<int> SendStripeTokenAsync(Vendor vendor, string code)
        {
            StripeSettings = await _context.StripeSettings.FirstOrDefaultAsync();
            var client = new StripeClient(StripeSettings.SecretKey);
            var service = new OAuthTokenService(client);
            var options = new OAuthTokenCreateOptions
            {
                GrantType = "authorization_code",
                Code = code,
            };

            OAuthToken response = null;
            try
            {
                response = service.Create(options);
            }
            catch (StripeException e)
            {
                if (e.StripeError != null && e.StripeError.Error == "invalid_grant") 
                {
                    return StatusCodes.Status400BadRequest;
                } 
                else 
                {
                    return StatusCodes.Status500InternalServerError;
                }
            }

            var connectedAccountId = response.StripeUserId;
            vendor.StripeAccountId = connectedAccountId;
            vendor.StripeState = "";
            await _context.SaveChangesAsync();

            return StatusCodes.Status200OK;
        }
    }
}