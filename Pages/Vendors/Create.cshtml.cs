using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Vendors
{
    public class CreateModel : PageModel 
    {
        private readonly DataContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public CreateModel(DataContext context, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager) 
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "Place Name")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Treat Type")]
            public string DessertType { get; set; }

            [Required]
            [Display(Name = "Place image url")]
            public string ImageUrl { get; set; }

            [Required]
            [StringLength(355)]
            [Display(Name = "Short Description")]
            public string Description { get; set; }

            [Display(Name = "Social Media Link")]
            public string SocialMediaLink { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public IActionResult OnGet() 
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync() 
        {
            if (!ModelState.IsValid) return Page();

            var vendor = new Vendor { 
                Name = Input.Name, 
                DessertType = Input.DessertType, 
                UserEmail = Input.Email , 
                ImagePath = Input.ImageUrl,
                Description = Input.Description,
                SocialMediaLink = Input.SocialMediaLink 
            };
            
            var user = await CreateUserAsync();
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "User already has a place registered.");
                return Page();
            }

            _context.Vendors.Add(vendor);
            await _context.SaveChangesAsync();

            await LogIn(user);

            return RedirectToPage("./Index");
        }

        private async Task<IdentityUser> CreateUserAsync()
        {
            if (await _userManager.FindByEmailAsync(Input.Email) != null)
            {
                return null;
            }

            var user = new IdentityUser { UserName = Input.Email, Email = Input.Email };
            var result = await _userManager.CreateAsync(user, Input.Password);
            if (result.Succeeded)
            {
                var role = await _roleManager.FindByNameAsync(Roles.User);
                if (role == null) 
                {
                    role = new IdentityRole { Name = Roles.User };
                    await _roleManager.CreateAsync(role);
                }

                await _userManager.AddToRoleAsync(user, role.Name);
            }
            else {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }

                return null;
            }

            return user;
        }

        private async Task LogIn(IdentityUser user) 
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, (await _userManager.GetRolesAsync(user)).FirstOrDefault())
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, 
                new ClaimsPrincipal(claimsIdentity));
        }
    }
}
