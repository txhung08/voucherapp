using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VoucherApp.Pages.Accounts
{
    public class EditModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;

        public EditModel(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Old Password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public IActionResult OnGet()
        {
            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var userEmail = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;
            if (ModelState.IsValid && userEmail != null)
            {
                var currentUser = await _userManager.FindByEmailAsync(userEmail);
                var updateResult = await _userManager.ChangePasswordAsync(currentUser, Input.OldPassword, Input.Password);
                if (!updateResult.Succeeded)
                {
                    foreach(var error in updateResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }

                Success = updateResult.Succeeded;
            }

            return Page();
        }
    }
}