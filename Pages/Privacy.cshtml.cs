﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages
{
    public class PrivacyModel : PageModel
    {
        private readonly DataContext _context;

        public PrivacyModel(DataContext context)
        {
            _context = context;
        }

        public Privacy Privacy { get; set; }

        public async Task<IActionResult> OnGet()
        {
            Privacy = await _context.Privacy.FirstOrDefaultAsync();

            return Page();
        }
    }
}
