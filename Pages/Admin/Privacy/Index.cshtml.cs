using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.Privacy
{
    public class IndexModel : PageModel
    {
        private DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            public string Content { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var privacy = await _context.Privacy.FirstOrDefaultAsync();

            if (privacy != null) 
            {
                Input = new InputModel
                {
                    Content = privacy.Content,
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var privacy = await _context.Privacy.FirstOrDefaultAsync();
            if (privacy == null)
            {
                privacy = new VoucherApp.Models.Privacy { 
                    Content = Input.Content
                };
                
                _context.Privacy.Add(privacy);
            }
            else 
            {
                privacy.Content = Input.Content;
            
                _context.Attach(privacy).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                Success = true;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}