using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Helpers;
using VoucherApp.Models;

namespace VoucherApp.Pages.Admin.Email
{
    public class IndexModel : PageModel
    {
        private readonly DataContext _context;
        
        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "SMTP server")]
            public string Server { get; set; }

            [Required]
            public int Port { get; set; }

            [Required]
            public string Username { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Send As")]
            public string SendAs { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var emailSettings = await _context.EmailSettings.FirstOrDefaultAsync();

            if (emailSettings != null)
            {
                Input = new InputModel
                {
                    Server = emailSettings.Server,
                    Port = emailSettings.Port,
                    Username = emailSettings.Username,
                    SendAs = emailSettings.SendAs
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var emailSettings = await _context.EmailSettings.FirstOrDefaultAsync();
            if (emailSettings == null)
            {
                emailSettings = new EmailSettings { 
                    Server = Input.Server,
                    Port = Input.Port,
                    Username = Input.Username, 
                    SendAs = Input.SendAs 
                };
                
                _context.EmailSettings.Add(emailSettings);
            }
            else 
            {
                emailSettings.Server = Input.Server;
                emailSettings.Port = Input.Port;
                emailSettings.Username = Input.Username;
                emailSettings.SendAs = Input.SendAs;

                _context.Attach(emailSettings).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                emailSettings.Password =  await Encryption.EncryptStringAsync(_context, Input.Password);

                await _context.SaveChangesAsync();

                Success = true;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}