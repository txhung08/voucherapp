using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Admin.Stripe
{
    public class IndexModel : PageModel
    {
        private readonly DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {            
            [Required]
            public string ClientId { get; set; }

            [Required]
            public string ApiKey { get; set; }

            [Required]
            public string SecretKey { get; set; }
            
            [Required]
            [Display(Name = "Charge percentage")]
            public double ChargePercentage { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var stripeSettings = await _context.StripeSettings.FirstOrDefaultAsync();

            if (stripeSettings != null)
            {
                Input = new InputModel { 
                    ClientId = stripeSettings.ClientId, 
                    ApiKey = stripeSettings.ApiKey, 
                    SecretKey = stripeSettings.SecretKey, 
                    ChargePercentage = stripeSettings.ChargePercentage 
                };
            }           

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var stripeSettings = await _context.StripeSettings.FirstOrDefaultAsync();
            if (stripeSettings == null)
            {
                stripeSettings = new StripeSettings { 
                    ClientId = Input.ClientId,
                    ApiKey = Input.ApiKey, 
                    SecretKey = Input.SecretKey,
                    ChargePercentage = Input.ChargePercentage 
                };
                
                _context.StripeSettings.Add(stripeSettings);
            }
            else 
            {
                stripeSettings.ClientId = Input.ClientId;
                stripeSettings.ApiKey = Input.ApiKey;
                stripeSettings.SecretKey = Input.SecretKey;
                stripeSettings.ChargePercentage = Input.ChargePercentage;

                _context.Attach(stripeSettings).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                Success = true;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}