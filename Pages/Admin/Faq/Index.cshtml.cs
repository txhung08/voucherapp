using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.Faq
{
    public class IndexModel : PageModel
    {
        private DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            public string Content { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var faq = await _context.Faq.FirstOrDefaultAsync();

            if (faq != null) 
            {
                Input = new InputModel
                {
                    Content = faq.Content,
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var faq = await _context.Faq.FirstOrDefaultAsync();
            if (faq == null)
            {
                faq = new VoucherApp.Models.Faq { 
                    Content = Input.Content
                };
                
                _context.Faq.Add(faq);
            }
            else 
            {
                faq.Content = Input.Content;
            
                _context.Attach(faq).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                Success = true;

                SiteSettingsStatic.Faq = faq.Content;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}