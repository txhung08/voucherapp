using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace VoucherApp.Pages.Admin.Accounts
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;

        public IndexModel(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public IList<IdentityUser> Users { get; set; }
        public string Error { get; set; }

        public async Task<IActionResult> OnGetAsync(string error)
        {
            Users = await _userManager.Users.ToListAsync();
            Error = error;

            return Page();
        }
    }
}