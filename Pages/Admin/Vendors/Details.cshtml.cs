using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.Vendors
{
    public class DetailsModel : PageModel
    {
        private readonly DataContext _context;

        public DetailsModel(DataContext context) 
        {
            _context = context;
        }

        public OutputModel Output { get; set; }

        public class OutputModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Owner { get; set; }

            [Display(Name = "Treat Type")]
            public string DessertType { get; set; }

            [Display(Name = "Image Path")]
            public string ImagePath { get; set; }

            [Display(Name = "Stripe Account Id")]
            public string StripeAccountId { get; set; }

            public string Description { get; set; }

            [Display(Name = "Social Media Link")]
            public string SocialMediaLink { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null) 
            {
                return RedirectToPage("/Errors/NotFound");
            }

            var vendor = await _context.Vendors.FirstOrDefaultAsync(m => m.Id == id);

            if (vendor == null)
            {
                return RedirectToPage("/Errors/NotFound");
            }

            Output = new OutputModel
            {
                Id = vendor.Id,
                Name = vendor.Name,
                Owner = vendor.UserEmail,
                DessertType = vendor.DessertType,
                ImagePath = vendor.ImagePath,
                StripeAccountId = vendor.StripeAccountId,
                Description = vendor.Description,
                SocialMediaLink = vendor.SocialMediaLink
            };

            return Page();
        } 
    }
}