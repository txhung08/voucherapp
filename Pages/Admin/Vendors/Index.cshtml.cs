using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Admin.Vendors
{
    public class IndexModel : PageModel 
    {
        private readonly DataContext _context;

        public IndexModel(DataContext context) 
        {
            _context = context;
        }

        public IList<Vendor> Vendors { get; set; }

        public async Task OnGetAsync() 
        {
            Vendors = await _context.Vendors.ToListAsync();
        }
    }
}