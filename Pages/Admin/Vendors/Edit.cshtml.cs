using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.Vendors
{
    public class EditModel : PageModel 
    {
        private readonly DataContext _context;

        public EditModel(DataContext context) 
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            public string Name { get; set; }

            [Required]
            [EmailAddress]
            public string Owner { get; set; }

            [Required]
            [Display(Name = "Treat Type")]
            public string DessertType { get; set; }

            [Required]
            [Display(Name = "Place image url")]
            public string ImagePath { get; set; }

            [Display(Name = "Stripe Account Id")]
            public string StripeAccountId { get; set; }

            [Required]
            [StringLength(355)]
            [Display(Name = "Short Description")]
            public string Description { get; set; }

            [Display(Name = "Social Media Link")]
            public string SocialMediaLink { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int? id) 
        {
            if (id == null) return RedirectToPage("/Errors/NotFound404");

            var vendor = await _context.Vendors.FirstOrDefaultAsync(m => m.Id == id);

            if (vendor == null) return RedirectToPage("/Errors/NotFound404");

            Input = new InputModel { 
                Id = vendor.Id, 
                Owner = vendor.UserEmail,
                Name = vendor.Name, 
                DessertType = vendor.DessertType, 
                StripeAccountId = vendor.StripeAccountId ,
                ImagePath = vendor.ImagePath,
                Description = vendor.Description,
                SocialMediaLink = vendor.SocialMediaLink
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var vendor = await _context.Vendors.FirstOrDefaultAsync(m => m.Id == Input.Id);
            vendor.Name = Input.Name;
            vendor.UserEmail = Input.Owner;
            vendor.ImagePath = Input.ImagePath;
            vendor.DessertType = Input.DessertType;
            vendor.StripeAccountId = Input.StripeAccountId;
            vendor.Description = Input.Description;
            vendor.SocialMediaLink = Input.SocialMediaLink;

            _context.Attach(vendor).State = EntityState.Modified;

            try 
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorExists(vendor.Id)) 
                {
                    return RedirectToPage("/Errors/NotFound");
                }
                else 
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool VendorExists(int id)
        {
            return _context.Vendors.Any(e => e.Id == id);
        }
    }
}