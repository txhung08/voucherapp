using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.TermsAndConditions
{
    public class IndexModel : PageModel
    {
        private DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            public string Content { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var termsAndConditions = await _context.TermsAndConditions.FirstOrDefaultAsync();

            if (termsAndConditions != null) 
            {
                Input = new InputModel
                {
                    Content = termsAndConditions.Content,
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var termsAndConditions = await _context.TermsAndConditions.FirstOrDefaultAsync();
            if (termsAndConditions == null)
            {
                termsAndConditions = new VoucherApp.Models.TermsAndConditions { 
                    Content = Input.Content
                };
                
                _context.TermsAndConditions.Add(termsAndConditions);
            }
            else 
            {
                termsAndConditions.Content = Input.Content;
            
                _context.Attach(termsAndConditions).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                Success = true;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}