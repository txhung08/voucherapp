using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Pages.Admin.Social
{
    public class IndexModel : PageModel
    {
        private DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            [Display(Name="Facebook Url")]
            public string FacebookUrl { get; set; }

            [Display(Name="Instagram Url")]
            public string InstagramUrl { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var socialMediaSettings = await _context.SocialMediaSettings.FirstOrDefaultAsync();

            if (socialMediaSettings != null) 
            {
                Input = new InputModel
                {
                    FacebookUrl = socialMediaSettings.FacebookUrl,
                    InstagramUrl = socialMediaSettings.InstagramUrl
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var socialMediaSettings = await _context.SocialMediaSettings.FirstOrDefaultAsync();
            if (socialMediaSettings == null)
            {
                socialMediaSettings = new VoucherApp.Models.SocialMediaSettings { 
                    FacebookUrl = Input.FacebookUrl,
                    InstagramUrl = Input.InstagramUrl
                };
                
                _context.SocialMediaSettings.Add(socialMediaSettings);
            }
            else 
            {
                socialMediaSettings.FacebookUrl = Input.FacebookUrl;
                socialMediaSettings.InstagramUrl = Input.InstagramUrl;
            
                _context.Attach(socialMediaSettings).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();

                SiteSettingsStatic.FacebookUrl = socialMediaSettings.FacebookUrl;
                SiteSettingsStatic.InstagramUrl = socialMediaSettings.InstagramUrl;

                Success = true;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}