using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;
using VoucherApp.Models;

namespace VoucherApp.Pages.Admin.Site
{
    public class IndexModel : PageModel
    {
        private DataContext _context;

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public bool Success { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "Site Name")]
            public string SiteName { get; set; }

            [Required]
            [Display(Name = "Site base url")]
            public string SiteUrl { get; set; }

            [Display(Name = "Site intro")]
            public string SiteIntro { get; set; }

            [Display(Name = "Site header image")]
            public string SiteImagePath { get; set; }

            [Display(Name = "Favicon CDN")]
            public string Favicon { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var siteSettings = await _context.SiteSettings.FirstOrDefaultAsync();

            if (siteSettings != null) 
            {
                Input = new InputModel
                {
                    SiteName = siteSettings.SiteName,
                    SiteUrl = siteSettings.SiteBaseUrl,
                    SiteIntro = siteSettings.SiteIntro,
                    SiteImagePath = siteSettings.SiteHeaderImage,
                    Favicon = siteSettings.SiteFavicon
                };
            }

            Success = false;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var siteSettings = await _context.SiteSettings.FirstOrDefaultAsync();
            if (siteSettings == null)
            {
                siteSettings = new SiteSettings { 
                    SiteName = Input.SiteName,
                    SiteBaseUrl = Input.SiteUrl, 
                    SiteIntro = Input.SiteIntro,
                    SiteHeaderImage = Input.SiteImagePath,
                    SiteFavicon = Input.Favicon
                };
                
                _context.SiteSettings.Add(siteSettings);
            }
            else 
            {
                siteSettings.SiteName = Input.SiteName;
                siteSettings.SiteBaseUrl = Input.SiteUrl;
                siteSettings.SiteIntro = Input.SiteIntro;
                siteSettings.SiteHeaderImage = Input.SiteImagePath;
                siteSettings.SiteFavicon = Input.Favicon;

                _context.Attach(siteSettings).State = EntityState.Modified;
            }

            try 
            {
                var result = await _context.SaveChangesAsync();
                Success = true;

                SiteSettingsStatic.SiteName = siteSettings.SiteName;
                SiteSettingsStatic.SiteUrl = siteSettings.SiteBaseUrl;
                SiteSettingsStatic.SiteDescription = siteSettings.SiteIntro;
                SiteSettingsStatic.Favicon = siteSettings.SiteFavicon;
            }
            catch (DbUpdateConcurrencyException)
            {
                Success = false;
                return StatusCode(500);
            }

            return Page();
        }
    }
}