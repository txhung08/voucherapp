// ******** SORT *********
const sortIcons = {
    up: `<svg class="bi bi-arrow-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
            <path fill-rule="evenodd" d="M7.646 2.646a.5.5 0 01.708 0l3 3a.5.5 0 01-.708.708L8 3.707 5.354 6.354a.5.5 0 11-.708-.708l3-3z" clip-rule="evenodd"/>
        </svg>`,
    down: ` <svg class="bi bi-arrow-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.646 9.646a.5.5 0 01.708 0L8 12.293l2.646-2.647a.5.5 0 01.708.708l-3 3a.5.5 0 01-.708 0l-3-3a.5.5 0 010-.708z" clip-rule="evenodd"/>
                <path fill-rule="evenodd" d="M8 2.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V3a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
            </svg>`,
    none: ""
}

$(".sort-by").on("click", (e) => {
    e.preventDefault();

    const _this = $(e.target);
    const icon = _this.find(".icon");
    const currentOrder = _this.data("order");

    switch(currentOrder) {
        case "up":
            _this.data("order", "down");
            _this.attr("data-order", "down");
            icon.html(sortIcons.down);
            break;

        case "down":
            _this.data("order", "none");
            _this.attr("data-order", "none");
            icon.html(sortIcons.none);
            break;

        default:
            _this.data("order", "up");
            _this.attr("data-order", "up");
            icon.html(sortIcons.up);
            break;
    }

    $(".vendor-card[data-filtered='false']").animate({ opacity: 0 }, 50).promise().then(() => sort(_this));
});

const sort = (e) => {
    if (!e) return;

    if (e.data("type") === "a-z") {
        sortBy(e.data("order"), "name");

        $("#sort-dessert-type").data("order", "none");
        $("#sort-dessert-type").find(".icon").html(sortIcons.none);
    }
    if (e.data("type") === "dessert") {
        sortBy(e.data("order"), "desserttype");
        
        $("#sort-a-z").data("order", "none");
        $("#sort-a-z").find(".icon").html(sortIcons.none);
    }
}

const sortBy = (order, name) => {
    let dataOrder;
    let dataName = name;

    switch (order) {
        case "up":
            dataOrder = -1;
            break;
        
        case "down":
            dataOrder = 1;
            break;

        default:
            dataName = "id";
            dataOrder = -1;
            break;
    }

    $(".vendor-card[data-filtered='false']").sort((a, b) => {
        if ($(a).data(dataName) <= $(b).data(dataName)) {
            return 1 * dataOrder;
        }
        else {
            return -1 * dataOrder;
        }
    }).promise().then((vendorsClone) => {
        $(".vendor-card[data-filtered='false']").each((k, v) => {
            const clone = $(vendorsClone[k]).clone();
            $(v).replaceWith(clone[0]);
        });
    }).then(() => {
        $(".vendor-card[data-filtered='false']").animate({ opacity: 1 }, 50);
    });
}

// ******** SEARCH *********
$(".main-toolbar #search-bar").on("input", (e) => {
    const _this = $(e.target);
    $(".vendor-card").filter((k, v) => {
        if ($(v).data("name").toLowerCase().includes(_this.val().toLowerCase()))
        {
            $(v).data("filtered", "false");
            $(v).attr("data-filtered", "false");
            $(v).parent().fadeIn(50);
        }
        else {
            $(v).data("filtered", "true");
            $(v).attr("data-filtered", "true");
            $(v).parent().fadeOut(50);
        }
    });

    if (!_this.val()) {
        sort($(".sort-by:not([data-order=none])"));
    }
});