﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

const HideSpinner = () => {
    $("body").removeClass("spinner-on");
    $(".page-spinner").removeClass("d-flex");
    $(".page-spinner").addClass("d-none");
};

const ShowSpinner = () => {
    $("body").addClass("spinner-on");
    $(".page-spinner").removeClass("d-none");
    $(".page-spinner").addClass("d-flex");
};
