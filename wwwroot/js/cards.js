$(".vendor-card").on("mouseenter", function (e) {
    const _this = $(this);

    _this.addClass("card-box-shadow");
});

$(".vendor-card").on("mouseleave", function (e) {
    const _this = $(this);

    _this.removeClass("card-box-shadow");
});