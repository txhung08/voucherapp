const setupStripe = () => {
    if (!$("#StripeApi").val()) return;

    var stripe = Stripe($("#StripeApi").val());

    var elements = stripe.elements();
    
    var style = {
      base: {
        color: "#32325d",
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#aab7c4"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      },
    };
    
    var cardElement = elements.create('card', {style: style});
    cardElement.mount('#card-element');
    
    $("#OrderForm").submit(async (e) => {
        e.preventDefault();

        if (!$("#OrderForm").valid()) return false;

        ShowSpinner();

        const result = await stripe.createPaymentMethod({
            type: 'card',
            card: cardElement,
            billing_details: {
                address: {
                    city: $("#Input_Address_City").val(),
                    country: $("#Input_Address_Country").val(),
                    line1: $("#Input_Address_Line1").val(),
                    line2: $("#Input_Address_Line2").val(),
                    postal_code: $("#Input_Address_PostalCode").val(),
                    state: $("#Input_Address_County").val()
                },
                email: $("#Input_Email").val(),
                name: $("#Input_Name").val(),
                phone: $("#Input_PhoneNumber").val()
            }
        })
        
        stripePaymentMethodHandler(result);
    });
    
    const stripePaymentMethodHandler = async (result) => {
        if (result.error) {
            HideSpinner();

            const dialog = $("#confirmDialog");
            dialog.find("#confirmDialogTitle").text("Error");
            dialog.find("#confirmDialogBody").text("Something went wrong. Please try again. " + JSON.stringify(result.error));
            dialog.find("#modal-confirm").hide();

            dialog.modal("show");
        } else {
            const res = await fetch('/pay', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    payment_method_id: result.paymentMethod.id,
                    amount: Math.floor($("#Input_Amount").val() * 100),
                    email: $("#Input_Email").val(),
                    vendorId: $("#Vendor_Id").val()
                })
            })
            const paymentResponse = await res.json();
            handleServerResponse(paymentResponse);
        }
    }
    
    const handleServerResponse = async (response) => {
        HideSpinner();
        if (response.error) { 
            const dialog = $("#confirmDialog");
            dialog.find("#confirmDialogTitle").text("Error");
            dialog.find("#confirmDialogBody").text(response.error);
            dialog.find("#modal-confirm").hide();

            dialog.modal("show");
        } 
        else if (response.requires_action) {
            ShowSpinner();
            const { error: errorAction, paymentIntent } = await stripe.handleCardAction(response.payment_intent_client_secret);
            
            if (errorAction) {
                HideSpinner();   

                const dialog = $("#confirmDialog");
                dialog.find("#confirmDialogTitle").text("Error");
                dialog.find("#confirmDialogBody").text(errorAction.message);
                dialog.find("#modal-confirm").hide();

                dialog.modal("show");
            } 
            else {
                ShowSpinner();

                const serverResponse = await fetch('/pay', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ 
                        payment_intent_id: paymentIntent.id,
                        amount: Math.floor($("#Input_Amount").val() * 100),
                        email: $("#Input_Email").val(),
                        vendorId: $("#Vendor_Id").val() 
                    })
                });
                handleServerResponse(await serverResponse.json());
            }
        }
        else {
            window.location.href = `/Vouchers/Confirmation?voucherCode=${response.voucherCode}`;
        }
    }
}

setupStripe();