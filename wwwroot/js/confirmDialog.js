$("a.confirm-dialog-trigger").on("click", (e) => {
    e.preventDefault();

    const _this = $(e.target);
    const dialog = $("#confirmDialog");
    dialog.find("#confirmDialogTitle").text(_this.data("title"));
    dialog.find("#confirmDialogBody").text(_this.data("body"));
    dialog.find("#confirmDialogForm").attr("action", _this.attr("href"));

    dialog.modal("show");
});