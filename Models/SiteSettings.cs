namespace VoucherApp.Models
{
    public class SiteSettings
    {
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string SiteBaseUrl { get; set; }
        public string SiteIntro { get; set; }
        public string SiteHeaderImage { get; set; }
        public string SiteFavicon { get; set; }
    }
}