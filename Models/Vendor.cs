namespace VoucherApp.Models
{
    public class Vendor 
    {
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public string Name { get; set; }
        public string DessertType { get; set; }
        public string ImagePath { get; set; }
        public string StripeAccountId { get; set; }
        public string StripeState { get; set; }
        public string Description { get; set; }
        public string SocialMediaLink { get; set; }
    }
}