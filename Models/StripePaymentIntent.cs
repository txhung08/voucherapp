namespace VoucherApp.Models
{
    public class StripePaymentIntent
    {
        public int Id { get; set; }
        public string Response { get; set; }
    }   
}