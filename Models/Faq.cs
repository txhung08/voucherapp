namespace VoucherApp.Models
{
    public class Faq
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}