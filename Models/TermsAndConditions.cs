namespace VoucherApp.Models
{
    public class TermsAndConditions
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}