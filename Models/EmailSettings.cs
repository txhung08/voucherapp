namespace VoucherApp.Models
{
    public class EmailSettings
    {
        public int Id { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public string SendAs { get; set; }
        public byte[] IV { get; set; }
    }
}