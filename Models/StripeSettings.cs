namespace VoucherApp.Models
{
    public class StripeSettings
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public double ChargePercentage { get; set; }
    }
}