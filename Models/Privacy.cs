namespace VoucherApp.Models
{
    public class Privacy
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}