namespace VoucherApp.Models
{
    public class SocialMediaSettings
    {
        public int Id { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }
    }
}