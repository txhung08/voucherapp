namespace VoucherApp.Models
{
    public enum VoucherStatus
    {
        Created,
        Confirmed,
        Claimed
    }

    public class Voucher
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string Code { get; set; }
        public string OwnerEmail { get; set; }
        public VoucherStatus Status { get; set; }

        public int VendorId { get; set; }
    }
}