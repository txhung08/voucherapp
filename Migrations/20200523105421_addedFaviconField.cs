﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VoucherApp.Migrations
{
    public partial class addedFaviconField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1e831b27-2a2c-4925-a6b5-033b5996c474");

            migrationBuilder.AddColumn<string>(
                name: "SiteFavicon",
                table: "SiteSettings",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c30a3170-fa5a-48bf-8f38-3d39db4ca63a", "858cdb7c-9614-4859-a5ca-9bf77b19344d", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c30a3170-fa5a-48bf-8f38-3d39db4ca63a");

            migrationBuilder.DropColumn(
                name: "SiteFavicon",
                table: "SiteSettings");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1e831b27-2a2c-4925-a6b5-033b5996c474", "094e2c50-f3cc-4b9b-870d-e06f3e79cb12", "Admin", "ADMIN" });
        }
    }
}
