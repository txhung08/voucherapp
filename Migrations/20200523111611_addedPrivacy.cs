﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VoucherApp.Migrations
{
    public partial class addedPrivacy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c30a3170-fa5a-48bf-8f38-3d39db4ca63a");

            migrationBuilder.CreateTable(
                name: "Privacy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Privacy", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d29084c2-da74-4e87-b70c-fda8c5405408", "e934db87-5e69-4aab-975d-0ab810f43afb", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Privacy");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d29084c2-da74-4e87-b70c-fda8c5405408");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c30a3170-fa5a-48bf-8f38-3d39db4ca63a", "858cdb7c-9614-4859-a5ca-9bf77b19344d", "Admin", "ADMIN" });
        }
    }
}
