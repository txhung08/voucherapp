using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Data;

namespace VoucherApp.Helpers
{
    public static class Encryption
    {
        private static readonly string KEY = "2A472D4B6150645367566B5970337336";

        public static async Task<byte[]> EncryptStringAsync(DataContext context, string text)
        {
            var key = Encoding.UTF8.GetBytes(KEY);
            byte[] result;

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var emailSettings = await context.EmailSettings.FirstOrDefaultAsync();
                        emailSettings.IV = aesAlg.IV;
                        await context.SaveChangesAsync();

                        result = msEncrypt.ToArray();
                    }
                }
            }

            return result;
        }

        public static string DecryptString(byte[] iv, byte[] cipherText)
        {
            var key = Encoding.UTF8.GetBytes(KEY);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }
    }
}