using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VoucherApp.Data;

namespace VoucherApp.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class VendorController : Controller
    {
        private readonly DataContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public VendorController(DataContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Remove(int id)
        {
            var vendor = await _context.Vendors.FindAsync(id);
            var user = await _userManager.FindByEmailAsync(vendor.UserEmail);

            await _userManager.DeleteAsync(user);

            _context.Vendors.Remove(vendor);
            await _context.SaveChangesAsync();

            return Redirect("~/Admin/Vendors");
        }
    }
}