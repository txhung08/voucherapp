using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using VoucherApp.Data;

namespace VoucherApp.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly DataContext _context;

        public AccountController(UserManager<IdentityUser> userManager, DataContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Redirect("~/");
        }

        [HttpPost]
        public async Task<IActionResult> Remove(string id)
        {
            if (await _userManager.Users.CountAsync() <= 1)
            {
                return Redirect("~/Admin/Accounts?error=Cannot delete last user.");
            }

            var user = await _userManager.FindByIdAsync(id);
            var vendor = await _context.Vendors.FirstAsync(v => v.UserEmail == user.Email);
            if (vendor != null)
            {
                return Redirect($"~/Admin/Accounts?error=Cannot remove User attached to a vendor {vendor.Name}.");
            }

            await _userManager.DeleteAsync(user);

            return Redirect("~/Admin/Accounts");
        }
    }
}