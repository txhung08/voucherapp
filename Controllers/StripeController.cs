using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Stripe;
using VoucherApp.Data;
using VoucherApp.Helpers;
using VoucherApp.Models;

namespace VoucherApp.Controllers
{
    public class ConfirmPaymentRequest
    {
        [JsonProperty("payment_method_id")]
        public string PaymentMethodId { get; set; }

        [JsonProperty("payment_intent_id")]
        public string PaymentIntentId { get; set; }

        public int Amount { get; set; }
        public string Email { get; set; }
        public int VendorId { get; set; }
    }

    [Route("/pay")]
    public class StripeController : Controller
    {
        private readonly DataContext _context;

        public StripeController(DataContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index([FromBody] ConfirmPaymentRequest request)
        {
            var paymentIntentService = new PaymentIntentService();
            PaymentIntent paymentIntent = null;

            var stripeSettings = await _context.StripeSettings.FirstOrDefaultAsync();
            var secret = stripeSettings.SecretKey;
            if (secret == null) return StatusCode(500, new { error = "Invalid payment settings. Please contact Admin." });

            var vendorStripeId = (await _context.Vendors.FirstOrDefaultAsync(v => v.Id == request.VendorId)).StripeAccountId;
            if (vendorStripeId == null) return StatusCode(500, new { error = "Vendor has no payment setup. Please contact Admin."});

            StripeConfiguration.ApiKey = secret;

            try
            {
                if (request.PaymentMethodId != null)
                {
                    var createOptions = new PaymentIntentCreateOptions
                    {
                        PaymentMethod = request.PaymentMethodId,
                        Amount = request.Amount,
                        Currency = "gbp",
                        ConfirmationMethod = "manual",
                        Confirm = true,
                        TransferData = new PaymentIntentTransferDataOptions
                        {
                            Amount = Convert.ToInt64(Math.Round(request.Amount - (request.Amount * (stripeSettings.ChargePercentage / 100)))),
                            Destination = vendorStripeId
                        }
                    };
                    paymentIntent = paymentIntentService.Create(createOptions);
                }
                if (request.PaymentIntentId != null)
                {
                    var confirmOptions = new PaymentIntentConfirmOptions{};
                    paymentIntent = paymentIntentService.Confirm(
                        request.PaymentIntentId,
                        confirmOptions
                    );
                }
            }
            catch (StripeException e)
            {
                return Json(new { error = e.StripeError.Message });
            }
            return await GeneratePaymentResponseAsync(paymentIntent, request);
        }

        private async Task<IActionResult> GeneratePaymentResponseAsync(PaymentIntent intent, ConfirmPaymentRequest request)
        {
            if (intent.Status == "requires_action" &&
                intent.NextAction.Type == "use_stripe_sdk")
            {
                return Json(new
                {
                    requires_action = true,
                    payment_intent_client_secret = intent.ClientSecret
                });
            }
            else if (intent.Status == "succeeded")
            {
                var voucherCode = await GenerateVoucherAsync(request);

                return Json(new {  voucherCode });
            }
            else
            {
                return StatusCode(500, new { error = "Invalid PaymentIntent status" });
            }
        }

        private async Task<string> GenerateVoucherAsync(ConfirmPaymentRequest request) 
        {
            var voucher = new Voucher
            {
                Amount = request.Amount,
                Code = await GenerateVoucherCodeAsync(),
                OwnerEmail = request.Email,
                Status = VoucherStatus.Created,
                VendorId = request.VendorId
            };

            await _context.Vouchers.AddAsync(voucher);
            await _context.SaveChangesAsync();

            try
            {
                await SendEmailsAsync(request, voucher);
            } 
            catch { }
            
            return voucher.Code;
        }

        private async Task<string> GenerateVoucherCodeAsync()
        {
            var random = new Random();
            char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();

            var code = "";
            do {
                code = Enumerable.Range(1, 13)
                    .Select(k => keys[random.Next(0, keys.Length - 1)])
                    .Aggregate("", (e, c) => e + c);
            } while(await _context.Vouchers.AnyAsync(v => v.Code == code));
            
            return code;
        }

        private async Task SendEmailsAsync(ConfirmPaymentRequest request, Voucher voucher)
        {
            var emailSettings = await _context.EmailSettings.FirstOrDefaultAsync();
            if (emailSettings == null) return;

            var vendor = await _context.Vendors.FirstOrDefaultAsync(v => v.Id == request.VendorId);
            var siteSettings = await _context.SiteSettings.FirstOrDefaultAsync();

            SmtpClient client = new SmtpClient(emailSettings.Server);
            client.Port = emailSettings.Port;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(emailSettings.Username, Encryption.DecryptString(emailSettings.IV, emailSettings.Password));

            SendToClient(client, request, emailSettings, siteSettings, vendor, voucher);
            SendToVendor(client, request, emailSettings, siteSettings, vendor, voucher);
        }

        private void SendToClient(SmtpClient client, ConfirmPaymentRequest request, EmailSettings emailSettings, SiteSettings siteSettings, Vendor vendor, Voucher voucher) 
        {
            var siteName = siteSettings?.SiteName ?? "Voucher App";
            var siteUrl = siteSettings?.SiteBaseUrl ?? "https://localhost:5001";
            var voucherUrl = $"{siteUrl}/Vouchers/Confirmation?voucherCode={voucher.Code}";

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(emailSettings.SendAs);
            mailMessage.To.Add(request.Email);
            mailMessage.Body = EmailTemplates.ForClient(siteName, siteSettings?.SiteHeaderImage, vendor.ImagePath, vendor.SocialMediaLink, request.Amount / 100, voucher.Code, voucherUrl);
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "Thank you for your support!";
            client.Send(mailMessage);
        }

        private void SendToVendor(SmtpClient client, ConfirmPaymentRequest request, EmailSettings emailSettings, SiteSettings siteSettings, Vendor vendor, Voucher voucher) 
        {
            var siteName = siteSettings?.SiteName ?? "Voucher App";
            var siteUrl = siteSettings?.SiteBaseUrl ?? "https://localhost:5001";
            var voucherUrl = $"{siteUrl}/Vouchers/Confirmation?voucherCode={voucher.Code}";

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(emailSettings.SendAs);
            mailMessage.To.Add(vendor.UserEmail);
            mailMessage.Body = EmailTemplates.ForVendor(siteName, siteSettings?.SiteHeaderImage, vendor.ImagePath, vendor.SocialMediaLink, request.Amount / 100, voucher.Code, voucherUrl);
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "A voucher has been ordered!";
            client.Send(mailMessage);
        }
    }
}