using System.Collections.Generic;

namespace VoucherApp.Data
{
    public static class CountryMappings
    {
        public static IList<Mapping> Countries {
            get {
                return new List<Mapping> {
                    new Mapping { CountryName = "Australia", CountryCode = "AU" },
                    new Mapping { CountryName = "Austria", CountryCode = "AT" },
                    new Mapping { CountryName = "Belgium", CountryCode = "BE" },
                    new Mapping { CountryName = "Brazil", CountryCode = "BR" },
                    new Mapping { CountryName = "Canada", CountryCode = "CA" },
                    new Mapping { CountryName = "Denmark", CountryCode = "DK" },
                    new Mapping { CountryName = "Finland", CountryCode = "FI" },
                    new Mapping { CountryName = "France", CountryCode = "FR" },
                    new Mapping { CountryName = "Germany", CountryCode = "DE" },
                    new Mapping { CountryName = "Hong Kong", CountryCode = "HK" },
                    new Mapping { CountryName = "Ireland", CountryCode = "IE" },
                    new Mapping { CountryName = "Japan", CountryCode = "JP" },
                    new Mapping { CountryName = "Luxembourg", CountryCode = "LU" },
                    new Mapping { CountryName = "Mexico", CountryCode = "MX" },
                    new Mapping { CountryName = "Netherlands", CountryCode = "NL" },
                    new Mapping { CountryName = "New Zealand", CountryCode = "NZ" },
                    new Mapping { CountryName = "Norway", CountryCode = "NO" },
                    new Mapping { CountryName = "Singapore", CountryCode = "SG" },
                    new Mapping { CountryName = "Spain", CountryCode = "ES" },
                    new Mapping { CountryName = "Sweden", CountryCode = "SE" },
                    new Mapping { CountryName = "Switzerland", CountryCode = "CH" },
                    new Mapping { CountryName = "United Kingdom", CountryCode = "GB" },
                    new Mapping { CountryName = "United States", CountryCode = "US" },
                    new Mapping { CountryName = "Italy", CountryCode = "IT" },
                    new Mapping { CountryName = "Portugal", CountryCode = "PT" }
                };
            }
        }

        public class Mapping 
        {
            public string CountryName { get; set; }
            public string CountryCode { get; set; }
        }
    }
}