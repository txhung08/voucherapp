namespace VoucherApp.Data
{
    public static class SiteSettingsStatic
    {
        public static string SiteName { get; set; }
        public static string SiteUrl { get; set; }
        public static string SiteDescription { get; set; }
        public static string SiteImage { get; set; }
        public static string Favicon { get; set; }
        public static string Faq { get; set; }
        public static string FacebookUrl { get; set; }
        public static string InstagramUrl { get; set; }
    }
}