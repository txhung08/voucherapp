using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace VoucherApp.Data
{
    public static class DbInitializer
    {
        public static async Task SeedAdminAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (await userManager.FindByNameAsync("admin@app.com") == null)
            {
                var user = new IdentityUser
                {
                    UserName = "admin@app.com",
                    Email = "admin@app.com",
                };

                await userManager.CreateAsync(user, "admin");

                if (await roleManager.FindByNameAsync(Roles.Admin) == null)
                {
                    var role = new IdentityRole
                    {
                        Name = Roles.Admin
                    };

                    await roleManager.CreateAsync(role);
                }

                if (await roleManager.FindByNameAsync(Roles.User) == null)
                {
                    var role = new IdentityRole
                    {
                        Name = Roles.User
                    };

                    await roleManager.CreateAsync(role);
                }

                await userManager.AddToRoleAsync(user, Roles.Admin);
            }
        }

        public static async Task SeedSettings(DataContext context)
        {
            var siteSettings = await context.SiteSettings.FirstOrDefaultAsync();
            if (siteSettings != null)
            {
                SiteSettingsStatic.SiteName = siteSettings.SiteName;
                SiteSettingsStatic.SiteUrl = siteSettings.SiteBaseUrl;
                SiteSettingsStatic.SiteDescription = siteSettings.SiteIntro;
                SiteSettingsStatic.SiteImage = siteSettings.SiteHeaderImage;
                SiteSettingsStatic.Favicon = siteSettings.SiteFavicon;
            }

            SiteSettingsStatic.Faq = (await context.Faq.FirstOrDefaultAsync())?.Content;

            var socialSettings = await context.SocialMediaSettings.FirstOrDefaultAsync();
            if (socialSettings != null)
            {
                SiteSettingsStatic.FacebookUrl = socialSettings.FacebookUrl;
                SiteSettingsStatic.InstagramUrl = socialSettings.InstagramUrl;
            }
        }
    }
}