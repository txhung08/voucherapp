using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VoucherApp.Models;

namespace VoucherApp.Data
{
    public class DataContext : IdentityDbContext<IdentityUser>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<StripeSettings> StripeSettings { get; set; }
        public DbSet<EmailSettings> EmailSettings { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<Privacy> Privacy { get; set; }
        public DbSet<TermsAndConditions> TermsAndConditions { get; set; }
        public DbSet<Faq> Faq { get; set; }
        public DbSet<SocialMediaSettings> SocialMediaSettings { get; set; }
    }
}