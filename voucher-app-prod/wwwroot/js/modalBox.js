$("a.modal-box-trigger").on("click", (e) => {
    e.preventDefault();

    const _this = $(e.target);
    const modal = $("#lgModal");
    modal.find(".modal-title").text(_this.data("title"));
    modal.find(".modal-body").html(_this.data("content"));

    modal.modal("show");
});